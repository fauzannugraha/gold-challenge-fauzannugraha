const { Client } = require('pg')

const client = new Client({
  user: 'postgres',
  password: '12345fgn',
  host: 'localhost',
  port: 5432,
  database: 'upload_db',
})

module.exports = client
