const express = require('express')
const app = express()
const cors = require('cors')
const client = require('./database.js')

// middleware

app.use(cors())
app.use(express.json()) // req.body

// upload task
app.post('/todo', async (req, res) => {
  try {
    const { description } = req.body
    const newTodo = await client.query(
      'INSERT INTO todo_table (description) VALUES ($1)',
      [description],
    )
    res.status(200).json(newTodo.rows)
  } catch (err) {
    res.send(err.message)
  }
})

// get task

app.get('/todo', async (req, res) => {
  try {
    const allTodo = await client.query('SELECT * FROM todo_table')
    res.json(allTodo.rows)
  } catch (err) {
    console.log(err.message)
  }
})

// get task by Id

app.get('/todo/:id', async (req, res) => {
  try {
    const { id } = req.params
    const getTodoById = await client.query(
      'SELECT * FROM todo_table WHERE id = $1',
      [id],
    )
    res.json(getTodoById.rows[0])
  } catch (err) {
    console.log(err.message)
  }
})

// update task
app.put('/todo/:id', async (req, res) => {
  try {
    const { id } = req.params
    const { description } = req.body
    const updateTodo = await client.query(
      'UPDATE todo_table SET description = $1 WHERE id = $2',
      [description, id],
    )
    res.json('Todo was updated')
  } catch (err) {
    console.log(err.message)
  }
})

//delete task
app.delete('/todo/:id', async (req, res) => {
  try {
    const { id } = req.params
    const deleteTodo = await client.query(
      'DELETE FROM todo_table WHERE id = $1',
      [id],
    )
    res.json('todo was deleted')
  } catch (err) {
    console.log(err.message)
  }
})

app.listen(5000, () => {
  console.log('server running on http://localhost:5000')
})

client.connect((err) => {
  if (err) {
    console.log(err.message)
  } else {
    console.log('connected')
  }
})
