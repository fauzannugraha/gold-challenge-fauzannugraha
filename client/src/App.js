/* eslint-disable */
import React, { Fragment } from 'react'
import './App.css'

//components
import InputTask from './components/InputTask'
import ListTask from './components/ListTask'

function App() {
  return (
    <Fragment>
      <div className="container">
        <InputTask />
        <ListTask />
      </div>
    </Fragment>
  )
}

export default App
