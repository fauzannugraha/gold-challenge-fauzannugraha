/* eslint-disable */
import React, { Fragment, useEffect, useState } from 'react'

import EditTask from './EditTask'

const ListTask = () => {
  //delete task function

  const deleteTask = async (id) => {
    try {
      const deleteTask = await fetch(`http://localhost:5000/todo/${id}`, {
        method: 'DELETE',
      })

      setTask(task.filter((task) => task.id !== id))
    } catch (err) {
      console.log(err.message)
    }
  }

  const [task, setTask] = useState([])

  const getTask = async () => {
    try {
      const response = await fetch('http://localhost:5000/todo')
      const jsonData = await response.json()

      setTask(jsonData)
    } catch (err) {
      console.log(err.message)
    }
  }
  useEffect(() => {
    getTask()
  }, [])

  console.log(task)
  return (
    <Fragment>
      <table class="table table-dark text-center mt-5">
        <thead>
          <tr>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {/* <tr>
            <td>John</td>
            <td>Doe</td>
            <td>john@example.com</td>
          </tr>
          <tr>
            <td>Mary</td>
            <td>Moe</td>
            <td>mary@example.com</td>
          </tr>
          <tr>
            <td>July</td>
            <td>Dooley</td>
            <td>july@example.com</td>
          </tr>*/}
          {task.map((task) => (
            <tr key={task.id}>
              <td>{task.description}</td>
              <td>
                <EditTask task={task} />
              </td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => deleteTask(task.id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Fragment>
  )
}

export default ListTask
