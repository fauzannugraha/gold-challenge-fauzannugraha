/* eslint-disable */
import React, { Fragment, useState } from 'react'

const InputTask = () => {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')

  const onSubmitForm = async (e) => {
    e.preventDefault()
    try {
      const body = { description }
      const response = await fetch('http://localhost:5000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      })
      window.location = '/'
      console.log(response)
    } catch (err) {
      console.log(err.message)
    }
  }

  return (
    <Fragment>
      <h1 className="text-center mt-5">Task List</h1>
      <form className="d-flex mt-5" onSubmit={onSubmitForm}>
        <input
          type="text"
          className="form-control"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          placeholder="What the task for today?"
        />
        <button className="btn btn-success">Add Task</button>
      </form>
    </Fragment>
  )
}

export default InputTask
